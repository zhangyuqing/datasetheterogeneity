setwd("~/yuqingz/breast-masomenos/validations")
library(simulatorZ)
library(gbm)
library(Hmisc)

source("getTrueModel_update.R")
source("changevar.R")
source("simTime_update.R")
source("00utils.R")
source("zmatrix_update.R")
source("funCV_update.R")
#source("masomenos_update.R")

source("cvSubsets.R")
source("plusminuscore.R")
source("rowCoxTest.R")

load("ExpressionSetsList_rm_new.RData")
esets <- esets[-1]
### filter genes with integrative correlation
filtered.esets <- geneFilter(esets, 0.26)
esets <- filtered.esets

step <- 100
balance.variables <- c("size", "age", "grade")
largeN <- 150
iterations <- 100
fold <- 4
set.seed(732)
y.vars <- list()
for(i in 1:length(esets)){  
  time <- esets[[i]]$dmfs.time
  time <- as.numeric(as.character(time))
  cens <- esets[[i]]$dmfs.cens
  cens <- as.numeric(as.character(cens))
  y.vars[[i]] <- Surv(time, cens)
}
#sapply(y.vars,nrow)

Z.list <- list()
CV <- CSV <- c()
truemod <- getTrueModel(esets=esets, y.vars=y.vars, 
                        parstep=100, balance.variables=balance.variables)
#sapply(truemod$survH,length)
#max(y.vars[[1]][,1])
b=1
while(b <= iterations){
  print(paste("iteration: ", b, sep=""))  
  simmodels <- simData(obj=esets, balance.variables=NULL,
                       n.samples=largeN, type="two-steps", y.vars=y.vars)
  #sapply(y.vars,nrow)
  #sapply(simmodels$y.vars,nrow)
  sim3.esets <- simTime(simmodels=simmodels, result=truemod,
                        original.yvars=y.vars) 
  Z.list[[b]] <- try(zmatrix(obj=sim3.esets$obj, 
                             y.vars=sim3.esets$y.vars, fold=fold,
                             trainingFun=plusMinus,covar=balance.variables))
  if(class(Z.list[[b]])=="try-error") next
  
  sum.cv <- 0
  for(i in 1:length(esets)){
    sum.cv <- sum.cv + Z.list[[b]][i, i]
  }
  CV[b] <- sum.cv / length(esets)
  CSV[b] <- (sum(Z.list[[b]]) - sum.cv) / (length(esets)*(length(esets)-1))
  b=b+1
}

result_3 <- list(CSV=CSV, CV=CV)
Z_list_3 <- Z.list
simsets_3 <- sim3.esets

save(result_3, Z_list_3, simsets_3, filtered.esets,
     file="rmCAL_breast_masomenos_filtergenes.RData")