library(survival)
library(metafor)
load("C:/Users/zhang/Desktop/reproduce-ovarian-masomenos/ExpressionSetsList_ovarian.RData")
balance.variables <- c("age_at_initial_pathologic_diagnosis", "debulking")
set.seed(732)
y.vars <- list()
for(i in 1:length(esets)){  
  y.vars[[i]] <- esets[[i]]$y
}
setnames <- names(esets)

forestplot <- function(esets, y, cov, formula=y ~ cov,
                       mlab="Overall", rma.method="FE", at=NULL,
                       xlab="Hazard Ratio",sname=setnames,...) {
  #esets <- esets[sapply(esets, function(x) probeset %in% featureNames(x))]
  coefs <- sapply(1:length(esets), function(i) {
    tmp <- list(y=y[[i]],
                X=t(exprs(esets[[i]])),
                cov=as.factor(pData(esets[[i]])[, cov])
    )
    summary(coxph(formula,data=tmp))$coefficients[1,c(1,3)]
  })
  
  res.rma <- metafor::rma(yi = coefs[1,], sei = coefs[2,],
                          method=rma.method) # fixed effect model
  # The effect of covariates estimated by any proportional hazards 
  # model can thus be reported as hazard ratios
  # yi:observed effect sizes (PH model coefficients, log hazard ratio)
  # sei: corresponding standard errors of the coefficients (se of log HR)
  if (is.null(at)) at <- log(c(0.25,1,4,20))
  forest.rma(res.rma, xlab=xlab, slab=sname,
             atransf=exp, at=at, mlab=mlab,...)
  return(res.rma)
}

res <- forestplot(esets=esets, y=y.vars, cov="age_at_initial_pathologic_diagnosis",at=log(c(0.5,1,2,4)),cex=1.8)
res <- forestplot(esets=esets, y=y.vars, cov="debulking",at=log(c(0.5,1,2,4)),cex=1.8)
