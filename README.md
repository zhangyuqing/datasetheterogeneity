## Dataset heterogeneity in the validation of prediction models across studies ##

To reproduce the results in the paper, fetch this repository using Git with the command below, or download from the "download repository" link at the url [Bitbucket repository--Dataset Heterogeneity](https://bitbucket.org/zhangyuqing/datasetheterogeneity):

    git clone https://zhangyuqing@bitbucket.org/zhangyuqing/datasetheterogeneity.git

### Prepare Data ###

***Breast Cancer Studies***  
(scripts in /breast-masomenos/cleaning/)

- Create a directory named *workspace/*, make folders *data/*, *scripts/*, *prepared/* and *validations/* under this directory.

- Copy all scripts from *cleaning/* in the repository into  *scripts/*, and all scripts in *simulations/* into *validations/*. Note that there should not be any folder under *scripts/* or *validations/*.

- Run *installNeededPackages.R* and *fetchData.R* to install packages needed during the experiments and download datasets. The fetched datasets will be under *data/*.

- Run *prepare.R* and *getEsetsList.R* in the order. Cleaned sets and the list of ExpressionSets will be under *prepared/*. There will be two lists of ExpressionSets. The one named *ExpressionSetsList_rm_new.RData* is the desired list.

- Copy *ExpressionSetsList_rm_new.RData* into *validations/* for simulations.

***Ovarian Cancer Studies***  
(scripts in /ovarian-masomenos/cleaning/)

- Install [*curatedOvarianData*](https://bioconductor.org/packages/release/data/experiment/html/curatedOvarianData.html) Bioconductor package

- Copy *createEsetList_yz.R* under *extData/* in the curatedOvarianData R library folder.

- Run *prepareOvar.R*. The cleaned list of datasets is called *ExpressionSetsList_ovarian.RData*.



### Simulations ###

Run the scripts in */simulations/* to repeat the simulations. Please refer to "Summary of files - for simulation" to learn which heterogeneity factors the scripts consider. Files not mentioned below will use the generated .RData files to visualize the results (draw boxplots, heatmaps, etc).

***Breast Cancer / Mas-o-menos***: Files with names starting with *"simulation-"* will perform the simulations, with results saved in the same folder.

***Breast Cancer / Ridge regression***: Files with names starting with *"ridge-"* will perform the simulations.

***Ovarian Cancer/ Mas-o-menos***: Files with names starting with *"ovarian-"* will perform the simulations.

***Mixed-effect Model***: Scripts are under */linear-model/*. Run *create_design_mat.R* to create design matrices from the simulation results. Then run the Rmarkdown script *mixed-effect-mod.rmd* to train the mixed-effect models.

-  **Important note about** ***UpdateFunctions***: These are functions that are not yet available in the simulatorZ package. **Copy all of them to directories available for the simulation scripts**. Please also change the working directories of the  simulation scripts as needed.



### Summary of files ###

- ***For cleaning data***:
    * *installNeededPackages.R* : Install needed packages.     
    * *fetchData.R* : Download breast cancer datasets from url [http://compbio.dfci.harvard.edu/pubs/sbtpaper/data.zip](http://compbio.dfci.harvard.edu/pubs/sbtpaper/data.zip)
    * *prepare.R* : For breast cancer. Select desired sets, find shared gene features, remove low variance genes.
    * *getEsetsList.R*: For breast cancer. Remove patients with missing values, construct a list of ExpressionSets.
    * *lintrans_cutoff.R*: Linear transformation used in *prepare.R*.
    * *createEsetList_yz.R*: A modified script for the generating the list of ovarian cancer datasets from curatedOvarianData package, where genes with low variances in every dataset is removed.
    * *prepareOvar.R*: cleaning script for ovarian studies

- ***For simulation***:
    * *XXX-control.R* : Baseline simulation.
    * *XXX-covariates.R*: Balancing covariates.
    * *XXX-filtergenes.R*: Filtering genes with Integrative Correlations (IC), and include roughly 1000 genes. Thresholds for IC are determined by grid search using *choose_filter_threshold.R*.
    * *XXX-filtergenes-XXcutoff.R*: Filtering genes with Integrative Correlations using fixed threshold on the IC.
    * *XXX-truemodel.R*: Using the same true model
    * *XXX-diffcoef.R*: Using different true coefficients and linear predictor, but the same cumulative hazard for simulating the survival time.
    * *XXX-combined.R*: Combination of all three sources.
    * *XXX-combined-balanceCov.R*: Combination of using identical true models and balancing clinical covariates.
    * *XXX-combined-filterGenes.R*: Combination of using identical true models and filtering genes with IC.
    * *mixed-effect-mod.Rmd*: Mixed-effect model. Design matrices are generated first by *create_design_mat.R*.
    * scripts under */removeCAL/*: Simulation scripts for the original breast cancer datasets with CAL removed.

- ***For visualization***:
    * *forestplots.R*: Draw forest plots to present Hazard Ratio of the covariates using the original studies. Used for Supplementary Figure 1.
    * *boxplots.R*: Generate boxplots in the paper, to compare the CV and CSV values in the scenarios above. Used for Figure 1 in the main paper and Supplementary Figure 3.
    * *heatmap.R*: Draw the heatmap to show the average C Index matrices generated with experimental and simulated sets. Used for Supplementary Figure 4.
    * *barplots.R*: Present the distribution of covariates before and after balancing, in each dataset and all sets combined. Used for Supplementary Figure 5.
    * *spagetti_plot.R*: To show the re-ordering of CSV ranks. Used for Supplementary Figure 6.

### Requirements ###
R/Bioconductor: Requires a recent enough version of Bioconductor to include BiocInstaller and has a release version of [*simulatorZ*](https://bioconductor.org/packages/release/bioc/html/simulatorZ.html). (R version >=3.0, Bioconductor version >= 3.0)
