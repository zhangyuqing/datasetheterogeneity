setwd("~/yuqingz/breast-masomenos/validations")
library(Hmisc)
library(simulatorZ)
library(gbm)
library(gplots)
library(ClassDiscovery)

source("getTrueModel_update.R")
source("changevar.R")
source("simTime_update.R")
source("00utils.R")
source("zmatrix_update.R")
source("funCV_update.R")
#source("masomenos_update.R")

source("cvSubsets.R")
source("plusminuscore.R")
source("rowCoxTest.R")

load("ExpressionSetsList_rm_new.RData")
step <- 100
balance.variables <- c("size", "age", "grade")
largeN <- 150
iterations <- 100
fold <- 4
set.seed(732)
y.vars <- list()
for(i in 1:length(esets)){  
  time <- esets[[i]]$dmfs.time
  time <- as.numeric(as.character(time))
  cens <- esets[[i]]$dmfs.cens
  cens <- as.numeric(as.character(cens))
  y.vars[[i]] <- Surv(time, cens)
}

Z.original <- zmatrix(obj=esets, y.vars=y.vars, fold=fold,
                      trainingFun=plusMinus,covar=balance.variables)

# heat1 <- Z.original
# pal<-blueyellow(15)[4:15]
# rownames(heat1)<-c("CAL", "MNZ", "TAM1", "TAM2","TRB", "UNT", "VDX")
# colnames(heat1)<-c("CAL", "MNZ", "TAM1", "TAM2","TRB", "UNT", "VDX")
# heatmap.2(heat1,scale='none',dendrogram='none',Rowv=NA,Colv=NA,
#           trace='none',col=pal,
#           breaks=c(0,0.4,0.5,0.53,0.56,0.6,0.64,0.68,0.74,0.8,0.85,0.9,1),
#           margins=c(5,5),lwid=c(0.7,3.8),lhei=c(0.7,3.8),key=FALSE)#keysize=5)


Z.list <- list()
truemod <- getTrueModel(esets=esets, y.vars=y.vars, 
                        parstep=100, balance.variables=balance.variables)
b=1
while(b <= iterations){
  print(paste("iteration: ", b, sep=""))  
  simmodels <- simData(obj=esets, balance.variables=NULL,
                       n.samples=largeN, type="one-step", y.vars=y.vars)
  #sapply(y.vars,nrow)
  #sapply(simmodels$y.vars,nrow)
  sim3.esets <- simTime(simmodels=simmodels, result=truemod,
                        original.yvars=y.vars) 
  Z.list[[b]] <- try(zmatrix(obj=sim3.esets$obj, 
                             y.vars=sim3.esets$y.vars, fold=fold,
                             trainingFun=plusMinus,covar=balance.variables))
  if(class(Z.list[[b]])=="try-error") next
  b=b+1
}
sum.Z <- matrix(rep(0, length(esets)*length(esets)), nrow=length(esets))
count.itr <- 0
for(b in 1:iterations){
  marker <- TRUE
  for(i in 1:nrow(Z.list[[b]])){
    for(j in 1:ncol(Z.list[[b]])){
      if(is.nan(Z.list[[b]][i, j])) marker <- FALSE
    }
  }
  if(marker){
    sum.Z <- sum.Z + Z.list[[b]]
    count.itr <- count.itr + 1
  }    
}
Z.simulated <- sum.Z / count.itr
print(Z.simulated)

# heat2 <- Z.simulated
# pal<-blueyellow(15)[4:15]
# rownames(heat2)<-c("CAL", "MNZ", "TAM1", "TAM2","TRB", "UNT", "VDX")
# colnames(heat2)<-c("CAL", "MNZ", "TAM1", "TAM2","TRB", "UNT", "VDX")
# heatmap.2(heat2,scale='none',dendrogram='none',Rowv=NA,Colv=NA,
#           trace='none',col=pal,
#           breaks=c(0,0.4,0.5,0.53,0.56,0.6,0.64,0.68,0.74,0.8,0.85,0.9,1),
#           margins=c(5,5),lwid=c(0.7,3.8),lhei=c(0.7,3.8),key=FALSE)
save(Z.original,Z.simulated,count.itr,file="heatmap.RData")