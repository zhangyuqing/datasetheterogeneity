changevar <- function
### change discrete variables into binary variables
(dat
### a data frame with columns as discrete variables
 ){
  var.names <- c()
  res.dat <- matrix(0,nrow=nrow(dat),ncol=1)  
  for(i in 1:ncol(dat)){
    if(class(dat[, i])=="factor") tmp <- levels(dat[, i])
    else tmp <- unique(dat[, i])
    tmp <- tmp[order(tmp)]
    if(length(tmp)<=2){
      res.dat <- cbind(res.dat, dat[, i])
      var.names <- c(var.names, colnames(dat)[i])
    }
    else{
      tmp.dat <- matrix(0,nrow=nrow(dat), ncol=length(tmp)-1)
      for(j in 1:(length(tmp)-1)){
        ii <- which(dat[,i]==tmp[j])
        tmp.dat[ii, j] <- 1
        tmp.dat[-ii, j] <- 0
      }
      var.names <- c(var.names, paste(colnames(dat)[i], 1:(length(tmp)-1), sep="_"))
      res.dat <- cbind(res.dat, tmp.dat)
    }
  }
  res.dat <- res.dat[,-1]
  rownames(res.dat) <- rownames(dat)
  colnames(res.dat) <- var.names
  return(res.dat)
  ### a data frame with all variables binarized
}